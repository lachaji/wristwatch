# Hand-based fine activity recognition

Goal is to study the feasibility of fine-grained activity recognition of hand gestures through a wristband/smartwatch sensor.

# Proof of concept

We can use a TicWatch E2 to extract accelerometer/gyroscopic and heart rate data:

![screen0](screenshot1.png) ![screen1](screenshot0.png)
